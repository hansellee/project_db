CREATE TABLE Tb_Buku(
	KodeBuku CHAR(3) PRIMARY KEY,
	JudulBuku VARCHAR(28),
	NamaPengarang VARCHAR(28)
)

CREATE TABLE Tb_Benda(
	KodeBenda CHAR(3) PRIMARY KEY,
	JudulBenda VARCHAR(28),
	NamaBenda VARCHAR(28)
)

CREATE TABLE Tb_Barang(
	KodeBarang CHAR(3) PRIMARY KEY,
	JudulBarang VARCHAR(28),
	NamaBarang VARCHAR(28)
)
