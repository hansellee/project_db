﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Entities.Entities.PostGre
{
    [Table("tb_barang")]
    public partial class TbBarang
    {
        [Key]
        [Column("kodebarang")]
        [StringLength(3)]
        public string Kodebarang { get; set; }
        [Column("judulbarang")]
        [StringLength(28)]
        public string Judulbarang { get; set; }
        [Column("namabarang")]
        [StringLength(28)]
        public string Namabarang { get; set; }
    }
}
