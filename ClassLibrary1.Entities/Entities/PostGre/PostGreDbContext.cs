﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Entities.Entities.PostGre
{
    public partial class PostGreDbContext : DbContext
    {
        //public PostGreDbContext()
        //{
        //}

        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbBarang> TbBarang { get; set; }
        public virtual DbSet<TbBenda> TbBenda { get; set; }
        public virtual DbSet<TbBuku> TbBuku { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=db_2;Username=postgres;Password=hansel2808;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbBarang>(entity =>
            {
                entity.HasKey(e => e.Kodebarang)
                    .HasName("tb_barang_pkey");

                entity.Property(e => e.Kodebarang).IsFixedLength();
            });

            modelBuilder.Entity<TbBenda>(entity =>
            {
                entity.HasKey(e => e.Kodebenda)
                    .HasName("tb_benda_pkey");

                entity.Property(e => e.Kodebenda).IsFixedLength();
            });

            modelBuilder.Entity<TbBuku>(entity =>
            {
                entity.HasKey(e => e.Kodebuku)
                    .HasName("tb_buku_pkey");

                entity.Property(e => e.Kodebuku).IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
