﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Entities.Entities.PostGre
{
    [Table("tb_benda")]
    public partial class TbBenda
    {
        [Key]
        [Column("kodebenda")]
        [StringLength(3)]
        public string Kodebenda { get; set; }
        [Column("judulbenda")]
        [StringLength(28)]
        public string Judulbenda { get; set; }
        [Column("namabenda")]
        [StringLength(28)]
        public string Namabenda { get; set; }
    }
}
